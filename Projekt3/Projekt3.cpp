// Projekt3.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "Projekt3.h"
#include <fstream>
#include <vector>
#include <cmath>
#include <string>

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

HWND hwndButton;
RECT wykres = { 50, 80, 600, 350 };
double skala_h = 1;
double skala_t = 1;
bool wykres_istnieje = false;

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

struct jeden_odczyt {
	double x;
	double y;
	double z;
};

struct punkt_ekstremum {
	double wartosc;
	int czas;
	bool maksimum;
};

struct punkt {
	double wartosc;
	int czas;
};

class dane {
public:
	void wczytaj_dane(char* plik, int pominiecie);
	void wpisz_do_wektora(double x, double y, double z);
	void pomin_kolumne();
	void odczyt_lini_akcelerometr();

	void m_wyznacz_ekstrema();
	void m_wyznacz_wysokosc();

	void rysuj_uklad(HDC hdc);
	void rysuj_czyszczenie(HWND hWnd, HDC &hdc, PAINTSTRUCT &ps, RECT *drawArea);
	void rysuj_wykres(HDC hdc, HWND hWnd);
	void rysuj_liczbe(HDC hdc, int x, int y, int wartosc);

	void resetuj_pozycje();
private:
	std::fstream plik;
	std::vector <jeden_odczyt> akcelerometr;
	std::vector <punkt_ekstremum> ekstrema;	
	std::vector <punkt> wysokosc;
	int piksel_wyswietlenia = 1;
};

void dane::wpisz_do_wektora(double ax, double ay, double az) {
	jeden_odczyt odczyt;

	odczyt.x = ax * 9.81;
	odczyt.y = ay * 9.81;
	odczyt.z = abs(az * 9.81);

	akcelerometr.push_back(odczyt);
}
void dane::wczytaj_dane(char* nazwa_pliku, int pominiecie) {

	if (akcelerometr.size() > 0) akcelerometr.clear();

	plik.open(nazwa_pliku, std::fstream::in);

	if (!plik.is_open()) {
		MessageBox(NULL, L"Nie mo�na otworzy� pliku", L":(", MB_ICONINFORMATION | MB_OKCANCEL);
	}
	else {
		for (int i = 0; i < pominiecie * 4; i++)
			if(!plik.eof()) pomin_kolumne();
		while (!plik.eof()) {
			odczyt_lini_akcelerometr();
		}
	}

	plik.close();
	plik.clear();
}
void dane::pomin_kolumne() {
	double pominiecie;

	for (int i = 0; i < 3; i++) {
		plik >> pominiecie;
	}
}
void dane::odczyt_lini_akcelerometr() {
	pomin_kolumne();

	double a, b, c;

	plik >> a;
	plik >> b;
	plik >> c;

	wpisz_do_wektora(a, b, c);

	pomin_kolumne();
	pomin_kolumne();
}

void dane::m_wyznacz_ekstrema() {
	if (ekstrema.size() > 0) ekstrema.clear();

	const int bramka = 20;
	if (akcelerometr.size() > 0) {
		punkt_ekstremum pierwsze;
		pierwsze.czas = 0;
		pierwsze.wartosc = akcelerometr[0].z;
		pierwsze.maksimum = true;
		ekstrema.push_back(pierwsze);

		bool ostatnie_maximum = true;
		double min_loc = akcelerometr[0].z;
		double max_loc = akcelerometr[0].z;

		int licznik_max = 0;
		int licznik_min = 0;

		for (int i = 0; i < akcelerometr.size(); i++) {
			if (ostatnie_maximum) {
				if (akcelerometr[i].z <= min_loc) {
					min_loc = akcelerometr[i].z;
					licznik_min = 0;
				}
				else
					licznik_min++;
				if (licznik_min == bramka) {
					punkt_ekstremum p;
					p.czas = i - licznik_min;
					p.wartosc = akcelerometr[i - licznik_min].z;
					p.maksimum = false;
					ekstrema.push_back(p);
					licznik_min = 0;
					max_loc = akcelerometr[i - licznik_min].z;
					ostatnie_maximum = false;
				}
			}
			else {
				if (akcelerometr[i].z >= max_loc) {
					max_loc = akcelerometr[i].z;
					licznik_max = 0;
				}
				else
					licznik_max++;
				if (licznik_max == bramka) {
					punkt_ekstremum p;
					p.czas = i - licznik_max;
					p.wartosc = akcelerometr[i - licznik_max].z;
					p.maksimum = true;
					ekstrema.push_back(p);
					licznik_max = 0;
					min_loc = akcelerometr[i - licznik_max].z;
					ostatnie_maximum = true;
				}
			}
		}
	}
}
void dane::m_wyznacz_wysokosc() {

	if (wysokosc.size() > 0) wysokosc.clear();
	if (ekstrema.size() > 0) {
		for (int i = 0; i < ekstrema.size() - 1; i++) {
			if (!ekstrema[i].maksimum) {
				double wyhylenie = acos(ekstrema[i].wartosc / 9.81);

				int przedzial = ekstrema[i + 1].czas - ekstrema[i].czas;

				for (int j = 0; j < przedzial; j++) {
					punkt p;
					p.czas = ekstrema[i].czas + j;
					p.wartosc = 1 - cos(wyhylenie*sin((((j + 1) * M_PI) / (2 * przedzial)) + (M_PI / 2)));
					wysokosc.push_back(p);
				}
			}
			else
			{
				double wyhylenie = acos(ekstrema[i + 1].wartosc / 9.81);

				int przedzial = ekstrema[i + 1].czas - ekstrema[i].czas;

				for (int j = 0; j < przedzial; j++) {
					punkt p;
					p.czas = ekstrema[i].czas + j;
					p.wartosc = 1 - cos(wyhylenie*sin((((j + 1) * M_PI) / (2 * przedzial))));
					wysokosc.push_back(p);
				}
			}

		}
	}
}

void dane::rysuj_uklad(HDC hdc)
{
	Graphics graphics(hdc);
	Pen pen(Color(255, 0, 0, 0));

	//Rysowanie granic

	graphics.DrawLine(&pen, 80, 100, 80, 300);
	graphics.DrawLine(&pen, 80, 300, 580, 300);

	//Podpisywanie osi

	TextOutW(hdc, 60, 70, L"H[cm]", 5);
	TextOutW(hdc, 600, 290, L"T[s]", 4);
	TextOutW(hdc, 750, 160, L"Lini do pomini�cia:", 19);
	
	//Dodawanie skali
	int licznik = 0;
	int wypisz = 10 / (5 * skala_h);
	if (wypisz == 0) wypisz = 1;

	for (int i = 1; i <= 200; i++) {
		int porownanie = 20 * skala_h;
		if (i % porownanie == 0) {
			graphics.DrawLine(&pen, 78, 300-i, 82, 300-i);
			licznik++;
			if(licznik % wypisz == 0)
				rysuj_liczbe(hdc, 55, 295 - i, licznik*10);
		}
	}

	licznik = 0;
	wypisz = skala_t+0.5; // Zaokr�glenie w g�r�
	if (wypisz < 1) wypisz = 1;

	for (int i = 1; i <= 500; i++) {
		int porownanie = 25 / skala_t;
		if (i % porownanie == 0) {
			graphics.DrawLine(&pen, 80+i, 298, 80+i, 302);
			licznik++;
			if (licznik % wypisz == 0)
				rysuj_liczbe(hdc, 75+i, 310, licznik);
		}
	}
}
void dane::rysuj_czyszczenie(HWND hWnd, HDC &hdc, PAINTSTRUCT &ps, RECT *drawArea) {
	InvalidateRect(hWnd, drawArea, TRUE);
}
void dane::rysuj_wykres(HDC hdc, HWND hWnd) {

	Graphics graphics(hdc);
	Pen pen(Color(255, 0, 0, 255));
	if (piksel_wyswietlenia > 0) {
		int numer_wiersza = skala_t*(piksel_wyswietlenia - 1);
		if (numer_wiersza < wysokosc.size()) {
			int poczatek_h = 300 - wysokosc[numer_wiersza].wartosc * skala_h * 100;

			numer_wiersza = skala_t*(piksel_wyswietlenia);
			int koniec_h = 300;
			if (numer_wiersza < wysokosc.size())
				koniec_h = 300 - wysokosc[numer_wiersza].wartosc * skala_h * 100;

			graphics.DrawLine(&pen, 79 + piksel_wyswietlenia, poczatek_h, 80 + piksel_wyswietlenia, koniec_h);
		}
	}
	piksel_wyswietlenia++;
	if (piksel_wyswietlenia >= 500)
	{
		piksel_wyswietlenia = 1;
		KillTimer(hWnd, ID_TIMER_1);
	}
}
void dane::rysuj_liczbe(HDC hdc, int x, int y, int wartosc) {
	std::wstring string_a = std::to_wstring(wartosc);
	LPCWSTR string_b = string_a.c_str();
	TextOutW(hdc, x, y, string_b, lstrlenW(string_b));
}

void dane::resetuj_pozycje() {
	piksel_wyswietlenia = 1;
}

dane globalne_wahadlo;

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

	GdiplusStartupInput gdiplusStartupInput;
	ULONG_PTR           gdiplusToken;
	GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_PROJEKT3, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_PROJEKT3));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

	GdiplusShutdown(gdiplusToken);

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_PROJEKT3));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_PROJEKT3);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   hwndButton = CreateWindow(TEXT("button"),                      // The class name required is button
	   TEXT("+"),                  // the caption of the button
	   WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,  // the styles
	   10, 100,                                  // the left and top co-ordinates
	   30, 30,                              // width and height
	   hWnd,                                 // parent window handle
	   (HMENU)ID_BUTTON_H_PLUS,                   // the ID of your button
	   hInstance,                            // the instance of your application
	   NULL);                               // extra bits you dont really need

   hwndButton = CreateWindow(TEXT("button"),                      // The class name required is button
	   TEXT("-"),                  // the caption of the button
	   WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,  // the styles
	   10, 160,                                  // the left and top co-ordinates
	   30, 30,                              // width and height
	   hWnd,                                 // parent window handle
	   (HMENU)ID_BUTTON_H_MINUS,                   // the ID of your button
	   hInstance,                            // the instance of your application
	   NULL);

   hwndButton = CreateWindow(TEXT("button"),                      // The class name required is button
	   TEXT("+"),                  // the caption of the button
	   WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,  // the styles
	   490, 355,                                  // the left and top co-ordinates
	   30, 30,                              // width and height
	   hWnd,                                 // parent window handle
	   (HMENU)ID_BUTTON_T_PLUS,                   // the ID of your button
	   hInstance,                            // the instance of your application
	   NULL);

   hwndButton = CreateWindow(TEXT("button"),                      // The class name required is button
	   TEXT("-"),                  // the caption of the button
	   WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,  // the styles
	   550, 355,                                  // the left and top co-ordinates
	   30, 30,                              // width and height
	   hWnd,                                 // parent window handle
	   (HMENU)ID_BUTTON_T_MINUS,                   // the ID of your button
	   hInstance,                            // the instance of your application
	   NULL);

   hwndButton = CreateWindow(TEXT("button"),                      // The class name required is button
	   TEXT("RYSUJ"),                  // the caption of the button
	   WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,  // the styles
	   900, 100,                                  // the left and top co-ordinates
	   150, 30,                              // width and height
	   hWnd,                                 // parent window handle
	   (HMENU)ID_BUTTON_RYSUJ,                   // the ID of your button
	   hInstance,                            // the instance of your application
	   NULL);

   hwndButton = CreateWindow(TEXT("button"),                      // The class name required is button
	   TEXT("WCZYTAJ"),                  // the caption of the button
	   WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,  // the styles
	   950, 150,                                  // the left and top co-ordinates
	   100, 30,                              // width and height
	   hWnd,                                 // parent window handle
	   (HMENU)ID_BUTTON_WCZYTAJ,                   // the ID of your button
	   hInstance,                            // the instance of your application
	   NULL);

   hwndButton = CreateWindow(TEXT("edit"),                      // The class name required is button
	   TEXT("0"),                  // the caption of the button
	   WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,  // the styles
	   900, 160,                                  // the left and top co-ordinates
	   50, 30,                              // width and height
	   hWnd,                                 // parent window handle
	   (HMENU)ID_TEXT_POMIN,                   // the ID of your button
	   hInstance,                            // the instance of your application
	   NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;
	HDC hdc;

    switch (message)
    {
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Parse the menu selections:
			switch (wmId)
			{
			case IDM_ABOUT:
				DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
				break;
			case IDM_EXIT:
				DestroyWindow(hWnd);
				break;
			case ID_BUTTON_RYSUJ: 
			{
				if (wykres_istnieje) {
					KillTimer(hWnd, ID_TIMER_1);
					globalne_wahadlo.resetuj_pozycje();
					globalne_wahadlo.rysuj_czyszczenie(hWnd, hdc, ps, &wykres);
					hdc = BeginPaint(hWnd, &ps);
					globalne_wahadlo.rysuj_uklad(hdc);
					EndPaint(hWnd, &ps);
				}
				globalne_wahadlo.m_wyznacz_ekstrema();
				globalne_wahadlo.m_wyznacz_wysokosc();
				wykres_istnieje = true;
				SetTimer(hWnd, ID_TIMER_1, 40*skala_t, 0); // czas w ms	
			}	
			break;
			case ID_BUTTON_WCZYTAJ: {
				HWND textbox = GetDlgItem(hWnd, ID_TEXT_POMIN);

				LPWSTR pominiecie = new wchar_t[100];
				GetWindowText(textbox, pominiecie, GetWindowTextLength(textbox)+1);

				int ilosc = _wtoi(pominiecie);
				globalne_wahadlo.wczytaj_dane("outputPendulumOrt01.log", ilosc);
			}
			break;
			case ID_BUTTON_H_PLUS:
			{
				if (wykres_istnieje) {
					KillTimer(hWnd, ID_TIMER_1);
					globalne_wahadlo.resetuj_pozycje();
				}
				skala_h += 0.2;
				if (skala_h > 3.1)
					skala_h = 3;
				globalne_wahadlo.rysuj_czyszczenie(hWnd, hdc, ps ,&wykres);
				hdc = BeginPaint(hWnd, &ps);
				globalne_wahadlo.rysuj_uklad(hdc);
				EndPaint(hWnd, &ps);
			}
			break;
			case ID_BUTTON_H_MINUS:
			{
				if (wykres_istnieje) {
					KillTimer(hWnd, ID_TIMER_1);
					globalne_wahadlo.resetuj_pozycje();
				}
				skala_h -= 0.2;
				if (skala_h < 0.3)
					skala_h = 0.4;
				globalne_wahadlo.rysuj_czyszczenie(hWnd, hdc, ps, &wykres);
				hdc = BeginPaint(hWnd, &ps);
				globalne_wahadlo.rysuj_uklad(hdc);
				EndPaint(hWnd, &ps);
			}
			break;
			case ID_BUTTON_T_PLUS:
			{
				if (wykres_istnieje) {
					KillTimer(hWnd, ID_TIMER_1);
					globalne_wahadlo.resetuj_pozycje();
				}
				skala_t += 0.2;
				if (skala_t > 2.7)
					skala_t = 2.6;
				globalne_wahadlo.rysuj_czyszczenie(hWnd, hdc, ps, &wykres);
				hdc = BeginPaint(hWnd, &ps);
				globalne_wahadlo.rysuj_uklad(hdc);
				EndPaint(hWnd, &ps);
			}
			break;
			case ID_BUTTON_T_MINUS:
			{
				if (wykres_istnieje) {
					KillTimer(hWnd, ID_TIMER_1);
					globalne_wahadlo.resetuj_pozycje();
				}
				skala_t -= 0.2;
				if (skala_t < 0.1)
					skala_t = 0.2;
				globalne_wahadlo.rysuj_czyszczenie(hWnd, hdc, ps, &wykres);
				hdc = BeginPaint(hWnd, &ps);
				globalne_wahadlo.rysuj_uklad(hdc);
				EndPaint(hWnd, &ps);
			}
			break;
			default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
		hdc = BeginPaint(hWnd, &ps);
			// TODO: Add any drawing code that uses hdc here...
		globalne_wahadlo.rysuj_uklad(hdc);
		EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
	case WM_TIMER: {
		switch (wParam)
		{
			case ID_TIMER_1: {
				hdc = GetDC(hWnd);
				globalne_wahadlo.rysuj_wykres(hdc, hWnd);
				ReleaseDC(hWnd, hdc);
			}	break;
		} 
	} break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}
